package ekipik;

/**
* Arbeitsauftrag-LS01-5 Aufgabe 1
*
* @author  MuFU
* @version 1.0
*/
public class LS01_5_A1 {
	
	public static void main(String[] args) {
		/*
		 * Aufgabe 1 in einer zeile zusammen gefasst, geht auch in mehreren
		 * */
		System.out.printf("%10s%n%5s%9s%n%5s%9s%n%10s", "**", "*", "*", "*", "*", "**");
	}
}
