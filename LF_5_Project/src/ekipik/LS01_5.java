package ekipik;

/**
* Arbeitsauftrag-LS01-5 Aufgabe 1 und 3
*
* @author  MuFU
* @version 1.0
*/
public class LS01_5 {
	
	public static void main(String[] args) {
		/*
		 * Aufgabe 1 in einer zeile zusammen gefasst, geht auch in mehreren
		 * */
		System.out.printf("%10s%n%5s%9s%n%5s%9s%n%10s", "**", "*", "*", "*", "*", "**");
		
		/*
		 * Aufgabe 2 Version: ohne leerstellen im string
		 * hier wird jedes einzelne symbol formatiert
		 * kann eventuell nicht genau das sein was 
		 * er als l�sung will
		 * */
		
		//Aufgabe 2 Version: ohne leerstellen im string
		System.out.printf("%-5s%s%19s%s%4s%n", "0!", "=", "","=", "1");
		System.out.printf("%-5s%-2s%-18s%s%4s%n", "1!", "=", "1","=", "1");
		System.out.printf("%-5s%-2s%-2s%-2s%-14s%s%4s%n", "2!", "=", "1", "*", "2","=", "2");
		System.out.printf("%-5s%-2s%-2s%-2s%-2s%-2s%-10s%s%4s%n", "3!", "=", "1", "*", "2", "*", "3","=", "6");
		System.out.printf("%-5s%-2s%-2s%-2s%-2s%-2s%-2s%-2s%-6s%s%4s%n", "4!", "=", "1", "*", "2", "*", "3", "*", "4","=", "24");
		System.out.printf("%-5s%-2s%-2s%-2s%-2s%-2s%-2s%-2s%-2s%-2s%-2s%s%4s%n", "5!", "=", "1", "*", "2", "*", "3", "*", "4", "*", "5","=", "120");
		
		/*
		 * Aufgabe 2 Version: leerstellen im string
		 * wenn er genau die 3 angegebenen Zahlen (5, 19, 4) sehen will 
		 * w�re das hier "eher" richtig. 
		 * */
		System.out.printf("%-5s%s%-19s%s%4s%n", "0!", "=", "", "=", "1");
		System.out.printf("%-5s%s%-19s%s%4s%n", "1!", "=", " 1 ", "=", "1");
		System.out.printf("%-5s%s%-19s%s%4s%n", "2!", "=", " 1 * 2 ", "=", "2");
		System.out.printf("%-5s%s%-19s%s%4s%n", "3!", "=", " 1 * 2 * 3 ", "=", "6");
		System.out.printf("%-5s%s%-19s%s%4s%n", "4!", "=", " 1 * 2 * 3 * 4 ", "=", "24");
		System.out.printf("%-5s%s%-19s%s%4s%n", "5!", "=", " 1 * 2 * 3 * 4 * 5 ", "=", "120");

		/*
		 * Aufgabe 3 einfachste version die m�glich ist
		 * */
		System.out.printf("%-12s|%10s%n","Fahrenheit", "Celsius");
		System.out.printf("%s%n","-----------------------");
		System.out.printf("%-12d|%10.2f%n", -20, -28.8889);
		System.out.printf("%-12d|%10.2f%n", -10, -23.3333);
		System.out.printf("%-12d|%10.2f%n", +0, -17.7778);
		System.out.printf("%-12d|%10.2f%n", +20, -6.6667);
		System.out.printf("%-12d|%10.2f%n", +30, -1.1111);
		
		/*
		 * Fazit: da Tennbush(?) krank ist wissen wir leider gottes auch nicht was er genau will als l�sung
		 * benutzt das hier mit vorsicht
		 * MuFu out 
		 * */
		
	}
}
