
public class Aufgabe5 {

	public static void main(String[] args) {
		int zahl = 95;
		System.out.println(zahlZuText(zahl));
	}
	
	//hier soll Ihr Quelltext hin
	public static String zahlZuText(int zahl) {
		String singleDigit[] = new String[10];
		String doubleDigit[] = new String[100];
		
		singleDigit[0] = "eins";
		singleDigit[1] = "ein";
		singleDigit[2] = "zwei";
		singleDigit[3] = "drei";
		singleDigit[4] = "vier";
		singleDigit[5] = "f�nf";
		singleDigit[6] = "sechs";
		singleDigit[7] = "sieben";
		singleDigit[8] = "acht";
		singleDigit[9] = "neun";
		
		doubleDigit[10] = "zehn";
		doubleDigit[20] = "zwanzig";
		doubleDigit[30] = "drei�ig";
		doubleDigit[40] = "vierzig";
		doubleDigit[50] = "f�nfzig";
		doubleDigit[60] = "sechzig";
		doubleDigit[70] = "siebzig";
		doubleDigit[80] = "achzig";
		doubleDigit[90] = "neunzig";
		
		if(zahl >= 1 && zahl < 10) {
			if(zahl == 1) {
				return singleDigit[0];
			}
			return singleDigit[zahl];
		}else if(zahl >= 10 && zahl < 20) {
			zahl -= 10;
			return singleDigit[zahl]+""+doubleDigit[10];
		}else if(zahl >= 20 && zahl < 30) {
			zahl -= 20;
			return singleDigit[zahl]+"und"+doubleDigit[20];
		}else if(zahl >= 30 && zahl < 40) {
			zahl -= 30;
			return singleDigit[zahl]+"und"+doubleDigit[30];
		}else if(zahl >= 40 && zahl < 50) {
			zahl -= 40;
			return singleDigit[zahl]+"und"+doubleDigit[40];
		}else if(zahl >= 50 && zahl < 60) {
			zahl -= 50;
			return singleDigit[zahl]+"und"+doubleDigit[50];
		}else if(zahl >= 60 && zahl < 70) {
			zahl -= 60;
			return singleDigit[zahl]+"und"+doubleDigit[60];
		}else if(zahl >= 70 && zahl < 80) {
			zahl -= 70;
			return singleDigit[zahl]+"und"+doubleDigit[70];
		}else if(zahl >= 80 && zahl < 90) {
			zahl -= 80;
			return singleDigit[zahl]+"und"+doubleDigit[80];
		}else if(zahl >= 90 && zahl < 100) {
			zahl -= 90;
			return singleDigit[zahl]+"und"+doubleDigit[90];
		}
		return "?";
	}
}
