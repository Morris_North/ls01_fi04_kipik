import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {
	   //While MIttelwert
	   //mittelWertWhile();
	   
	   //For Mittlewert
	   mittelWertFor();
	   
	   //Normale MIttelwert Berecnung
	   //mittelWertNormal();
   }
   
   public static void mittelWertFor() {
	   Scanner scan = new Scanner(System.in);
	   double ergebnis = 0;
	   
	   System.out.print("Anzahl: ");
	   int anzahl = scan.nextInt();
	   
	   for(int i = 1; i <= anzahl; i++) {
		   System.out.print("Zahl: ");
		   ergebnis += scan.nextDouble();
	   }
	   ergebnis += ergebnis / anzahl;
	   System.out.printf("Mittelwert: %.2f", ergebnis);
   }
   
   public static void mittelWertWhile() {
	   Scanner scan = new Scanner(System.in);
	   double input = 0;
	   double ergebnis = 0;
	   int count = 0;
	   while(input != 4711) {
		   System.out.print("ZahL: ");
		   input = scan.nextDouble();
		   if(input != 4711) {
			   ergebnis += input;
			   count ++;
		   }
	   }
	   
	   ergebnis = ergebnis / count;
	   System.out.printf("Mittelwert: %.2f",ergebnis);
   }
   
   public static void mittelWertNormal() {
	   // (E) "Eingabe"
	      // Werte für x und y festlegen:
	      // ===========================
	      double x = 2.0;
	      double y = 4.0;
	      double m;
	      
	      // (V) Verarbeitung
	      // Mittelwert von x und y berechnen: 
	      // ================================
	      m = mittlewert(2.0, 4.0);
	      
	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	      // =================================
	      ausgabe(x, y, m);
   }
   
   public static double mittlewert(double x, double y) {
	   double ergebnis = (x+y)/2.0;
	   return ergebnis;
   }
   
   public static void ausgabe(double x, double y, double m) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
}
