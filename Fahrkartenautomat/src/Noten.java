import java.util.Scanner;

public class Noten {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Note: ");
		int note = scan.nextInt();
		switch(note) {
			case 1: {
				System.out.println("Sehr gut");
				break;
			}
			case 2: {
				System.out.println("Gut");
				break;
			}
			case 3: {
				System.out.println("Befriedigend");
				break;
			}
			case 4: {
				System.out.println("Ausreichend");
				break;
			}
			case 5: {
				System.out.println("Mangelhaft");
				break;
			}
			case 6: {
				System.out.println("Ungenügend");
				break;
			}
			default: {
				System.out.println("Das ist keine Note.");
				break;
			}
		}
	}
}