import java.util.Scanner;
import java.util.*;

public class Sortiere_abc {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Bitte 3 Zeichen eingeben (Buchstabe, Zahl)");
		System.out.print("Erstes: ");
		String a = scan.next();
		System.out.print("Zweites: ");
		String b = scan.next();
		System.out.print("Drittes: ");
		String c = scan.next();
		String[] zeichen = {a, b, c};
		
		Arrays.sort(zeichen);
		
		for(String z : zeichen ){
			System.out.println(z);

		}
	}
}