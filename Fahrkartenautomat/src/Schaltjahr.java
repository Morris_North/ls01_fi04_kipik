import java.util.Scanner;

public class Schaltjahr {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Jahr: ");
		int jahr = scan.nextInt();
		
		if((jahr % 4) == 0) {
		    if((jahr % 100) == 0 && (jahr % 400) != 0) {
		    	System.out.println(jahr+" ist kein Schaltjahr.");
		    }else {
		    	System.out.println(jahr+" ist ein Schaltjahr.");
		    }
		}else {
			System.out.println(jahr+" ist kein Schaltjahr.");
		}
	}
}