import java.util.Scanner;

public class BMI {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Gewicht (kg): ");
		float weight = scan.nextFloat();
		System.out.print("Gr��e (cm): ");
		float height = scan.nextFloat();
		System.out.print("Geschlecht? (w/m): ");
		String sex = scan.next();
		
		height = height / 100;
		
		float bmi = weight / (height*height);

		switch(sex) {
			case "m": {
				if(bmi < 20) {
					System.out.println("Untergewicht.");
				}else if(bmi >= 20 && bmi <= 25) {
					System.out.println("Normalgewicht.");
				}else if(bmi > 25) {
					System.out.println("�bergewicht.");
				}
				break;
			}
			case "w": {
				if(bmi < 19) {
					System.out.println("Untergewicht.");
				}else if(bmi >= 19 && bmi <= 24) {
					System.out.println("Normalgewicht.");
				}else if(bmi > 24) {
					System.out.println("�bergewicht.");
				}
				break;
			}
		}
		
	}
}
