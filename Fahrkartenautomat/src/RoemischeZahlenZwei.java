import java.util.Scanner;

public class RoemischeZahlenZwei {
	static int values[] = new int[255];

	public static void main(String[] args) {
		initLookUp();
		Scanner scan = new Scanner(System.in);

		System.out.print("R�mische Zahl: ");
		String zeichen = scan.next();

		int resultat = parseRoman(zeichen.toUpperCase());
		if(resultat == -1) {
			System.out.println("Keine R�mische Zahl");
		}else {
			System.out.println("Ergebnis: "+resultat);
		}
	}

	public static int parseRoman(String zeichen) {
		int resultat = 0;
		
		for(int i = 0; i < zeichen.length(); i++) {
			int part1 = values[zeichen.charAt(i)];
			if(part1 == 0) {
				//keine r�mischen zeichen
				return -1;
			}
			if(i + 1 < zeichen.length()) {
				
				int part2 = values[zeichen.charAt(i + 1)];
				
				if(part1 >= part2) {
					resultat = resultat + part1;
				}else {
					resultat = resultat + part2 - part1;
					i++;
				}
			}else {
				resultat = resultat + part1;
			}
		}
		return resultat;
	}
	
	public static void initLookUp() {
		values['I'] = 1;
		values['V'] = 5;
		values['X'] = 10;
		values['L'] = 50;
		values['C'] = 100;
		values['D'] = 500;
		values['M'] = 1000;
		// Error on 0
	}
}