import java.util.Scanner;

public class Rabattsystem {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Bestellwert: ");
		double bestellWert = scan.nextDouble();
		
		double betrag = 0;
		if(bestellWert > 0 && bestellWert <= 100) {
			betrag = bestellWert * 1.19 * 0.9;
		}else if(bestellWert > 100 && bestellWert <= 500) {
			betrag = bestellWert * 1.19 * 0.85;
		}else if(bestellWert > 500) {
			betrag = bestellWert * 1.19 *0.8;
		}
		
		System.out.printf("Bestellwert: %.2f �",betrag);
	}
}
